# Release.X (C) Nuno Ferreira and contributors - Licensed under GNU AGPLv3+

rm db.sqlite3 
rm -r projects/migrations

python3 manage.py makemigrations projects

python3 manage.py migrate

python3 manage.py shell < update_database.py

python3 manage.py runserver

