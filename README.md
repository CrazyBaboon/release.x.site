![alt text](release.X_logo_small.png)

## The Free and Open Source Portal

Release.X is a simple and minimal website striving to give up-to-date information on Free Software releases.

## License

Release.X - (C) Nuno Ferreira and contributors - Licensed under the GNU AGPLv3+

Release.X logotype - (C) Nuno Ferreira - Licensed under the Creative Commons Attribution 4.0 license

## How to start Release.X 

open a terminal in your **release.x** folder and type:

`./deploy.sh` - this will update your database and start the python server.

Once this is done, open firefox or chromium and go to http://127.0.0.1:8000/.
