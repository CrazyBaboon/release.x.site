# Release.X (C) Nuno Ferreira and contributors - Licensed under GNU AGPLv3+

from projects.models import Project
from datetime import datetime

import requests
from bs4 import BeautifulSoup
import re

# variable to count number of projects:
dummy_count = 0


################### 1 - GIMP ###############################################

html = requests.get('https://en.wikipedia.org/wiki/GIMP')

# turn the HTML into a beautiful soup text object
soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('<td>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

release_date = release_date[1:]

q = Project(name = "GIMP", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Raster graphics editor", category="Graphics", license = 'GNU GPL v3+', repository = 'https://gitlab.gnome.org/GNOME/gimp', website = 'https://www.gimp.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 2 - BLENDER ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Blender_(software)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[8]

try:
    version_number = re.search('<td>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

release_date = release_date[1:]

q = Project(name = "Blender", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "3D computer graphics software", category="Graphics", license = 'GNU GPL v2+', repository = 'https://git.blender.org/blender.git', website = 'https://www.blender.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 3 - INKSCAPE ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Inkscape')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[8]

try:
    version_number = re.search('<td>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

release_date = release_date[1:]

q = Project(name = "Inkscape", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Vector graphics editor", category="Graphics", license = 'GNU GPL v3+', repository = 'https://gitlab.com/inkscape/inkscape', website = 'https://inkscape.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################# 4 - AUDACITY ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Audacity_(audio_editor)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('<td>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

release_date = release_date[1:]

q = Project(name = "Audacity", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Digital audio recorder", category="Audio", license = 'GNU GPL v2', repository = 'https://github.com/audacity/audacity', website = 'https://www.audacityteam.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 5 - LIBREOFFICE ###############################################

html = requests.get('https://en.wikipedia.org/wiki/LibreOffice')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('<br/>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "LibreOffice", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Office suite", category="Office", license = 'MPL v2 + others', repository = 'git clone git://gerrit.libreoffice.org/core', website = 'https://www.libreoffice.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 6 - GNU OCtave ###############################################

html = requests.get('https://en.wikipedia.org/wiki/GNU_Octave')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('"margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "GNU Octave", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Numerical computation software", category="Science", license = 'GNU GPL v3+', repository = 'hg.savannah.gnu.org/hgweb/octave', website = 'https://www.gnu.org/software/octave/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 7 - GIT ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Git')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('<div style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Git", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Version-control system", category="Development", license = 'GNU GLP v2', repository = 'git.kernel.org/pub/scm/git/git.git/', website = 'https://git-scm.com/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 8 - SUPERTUXKART ###############################################

html = requests.get('https://en.wikipedia.org/wiki/SuperTuxKart')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/(.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

release_date = release_date[1:]

q = Project(name = "SuperTuxKart", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Kart racing game", category="Games", license = 'GNU GPL v3', repository = 'https://github.com/supertuxkart/stk-code.git', website = 'https://supertuxkart.net/Main_Page')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 9 - GCC ###############################################

html = requests.get('https://en.wikipedia.org/wiki/GNU_Compiler_Collection')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('margin:0px;">(.+?)<sup class', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "GCC", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "GNU Compiler Collection", category="Development", license = 'GNU GPL v3+', repository = 'gcc.gnu.org/viewcvs/gcc/', website = 'https://gcc.gnu.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 10 - Marble ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Marble_(software)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('style="margin:0px;">(.+?) ', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Marble", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %Y, %d').isoformat(), description = "Virtual globe application", category="Geography", license = 'GNU LGPL', repository = 'anongit.kde.org/marble.git', website = 'https://marble.kde.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 11 - FlightGear ###############################################

html = requests.get('https://en.wikipedia.org/wiki/FlightGear')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('style="margin:0px;">(.+?)<sup', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "FlightGear", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Flight simulator", category="Games", license = 'GNU GPL v2', repository = 'git.code.sf.net/p/flightgear/flightgear', website = 'http://home.flightgear.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 12 - LMMS ###############################################

html = requests.get('https://en.wikipedia.org/wiki/LMMS')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "LMMS", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Linux MultiMedia Studio", category="Audio", license = 'GNU GPL v2', repository = 'https://github.com/LMMS/lmms.git', website = 'https://lmms.io/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 13 - ARDOUR ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Ardour_(software)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[12]

try:
    version_number = re.search('<td>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search(' <small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Ardour", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Digital audio workstation", category="Audio", license = 'GNU GPL v2+', repository = 'git.ardour.org/ardour/ardour.git', website = 'https://ardour.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 14 - KRITA ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Krita')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('<td>(.+?)<sup class="ref', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search(' <small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Krita", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Digital painting program", category="Graphics", license = 'GNU GPL v3', repository = 'https://anongit.kde.org/krita.git', website = 'https://krita.org/en/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 15 - MYPAINT ###############################################

html = requests.get('https://en.wikipedia.org/wiki/MyPaint')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[8]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search(' / (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "MyPaint", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Digital painting program", category="Graphics", license = 'GNU GPL v2+', repository = 'https://github.com/mypaint/mypaint.git', website = 'http://mypaint.org/about/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 16 - GNUMERIC ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Gnumeric')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('<td>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search(' <small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Gnumeric", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Spreadsheet program", category="Office", license = 'GNU GPL v2+', repository = 'git.gnome.org/browse/gnumeric/', website = 'http://www.gnumeric.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 17 - LINUX ###############################################

html = requests.get('https://www.kernel.org/')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[20]
coord_tmp_2 = soup.find_all('td')[21]


try:
    version_number = re.search('<strong>(.+?)</strong></td>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<td>(.+?)</td>', str(coord_tmp_2)).group(1)
    release_date = release_date[2:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Linux", latest_version = version_number, release_date = datetime.strptime(release_date, '%y-%m-%d').isoformat(), description = "Monolithic Unix-like kernel", category="Operating System", license = 'GNU GPL v2', repository = 'https://github.com/torvalds/linux.git', website = 'https://www.kernel.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 18 - RIGS OF RODS ###############################################

html = requests.get('https://de.wikipedia.org/wiki/Rigs_of_Rods')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[19]

try:
    version_number = re.search('#AAAAAA;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)</small>', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

release_date = release_date[1:-1]


q = Project(name = "Rigs of Rods", latest_version = version_number, release_date = datetime.strptime(release_date, '%d. %B %Y').isoformat(), description = "Soft-body physics vehicle-simulator", category="Games", license = 'GNU GPL v3', repository = 'github.com/RigsOfRods/rigs-of-rods', website = 'https://www.rigsofrods.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 19 - SCRIBUS ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Scribus')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('style="margin:0px;">(.+?)<sup class="reference', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Scribus", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Layout publishing software", category="Graphics", license = 'Various', repository = 'sourceforge.net/p/scribus/trunk/ci/master/tree/', website = 'https://www.scribus.net/category/about/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 20 - PENCIL 2D ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Pencil2D')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[7]

try:
    version_number = re.search('"margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Pencil 2D", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Cartoon drawing program", category="Graphics", license = 'GNU GPL v2', repository = 'https://github.com/pencil2d/pencil.git', website = 'https://www.pencil2d.org/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 21 - STELLARIUM ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Stellarium_(software)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[8]

try:
    version_number = re.search('"margin:0px;">(.+?)<sup', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Stellarium", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Planetarium program", category="Science", license = 'GNU GPL v2', repository = 'github.com/Stellarium/stellarium', website = 'https://stellarium.org/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 22 - MIXXX ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Mixxx')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('"margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Mixxx", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "DJ software", category="Audio", license = 'GNU GPL v2+', repository = 'https://github.com/mixxxdj/mixxx.git', website = 'https://www.mixxx.org/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 23 - CELESTIA ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Celestia')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[7]

try:
    version_number = re.search('margin:0px;">(.+?)<small>', str(coord_tmp)).group(1)
    version_number = version_number[:5]
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Celestia", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Real-time 3D visualization of space", category="Science", license = 'GNU GPL v2+', repository = 'https://github.com/CelestiaProject/Celestia.git', website = 'https://celestia.space/index.html')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 24 - MUSESCORE ###############################################

html = requests.get('https://en.wikipedia.org/wiki/MuseScore')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "MuseScore", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Sheet music software", category="Audio", license = 'GNU GPL v2', repository = 'https://github.com/musescore/MuseScore.git', website = 'https://musescore.org/en')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 25 - VIM ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Vim_(text_editor)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('<td>(.+?)<sup class', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Vim", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Text editor", category="Development", license = 'Vim license', repository = 'https://github.com/vim/vim.git', website = 'https://www.vim.org/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 26 - NANO ###############################################

html = requests.get('https://en.wikipedia.org/wiki/GNU_nano')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "GNU Nano", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Text editor", category="Development", license = 'GNU GPL v3+', repository = 'git.savannah.gnu.org/cgit/nano.git', website = 'https://www.nano-editor.org/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 27 - SPYDER ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Spyder_(software)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Spyder", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "IDE for scientific programming with Python", category="Science", license = 'MIT', repository = 'https://github.com/spyder-ide/spyder.git', website = 'https://www.spyder-ide.org/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 28 - LYX ###############################################

html = requests.get('https://en.wikipedia.org/wiki/LyX')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Lyx", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Document processing based on LATEX", category="Office", license = 'GNU GPL', repository = 'git.lyx.org/lyx.git', website = 'https://www.lyx.org/Home')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 29 - DARKTABLE ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Darktable')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[7]

try:
    version_number = re.search('margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Darktable", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Raw image developer", category="Graphics", license = 'GNU GPL v3+', repository = 'https://github.com/darktable-org/darktable.git', website = 'https://www.darktable.org/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 30 - RAWTHERAPEE ###############################################

html = requests.get('https://en.wikipedia.org/wiki/RawTherapee')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "RawTherapee", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Raw image developer", category="Graphics", license = 'GNU GPL v3+', repository = 'https://github.com/Beep6581/RawTherapee', website = 'https://rawtherapee.com/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 31 - FIREFOX ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Firefox')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('Standard</th><td>(.+?) /', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Firefox", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Web browser", category="Internet", license = 'MPL 2.0', repository = 'https://hg.mozilla.org/mozilla-central/', website = 'https://www.mozilla.org/en-US/firefox/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 32 - WATERFOX ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Waterfox')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('Desktop</th><td>(.+?) /', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Waterfox", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Web browser", category="Internet", license = 'MPL 2.0', repository = 'https://github.com/MrAlex94/Waterfox.git', website = 'https://www.waterfoxproject.org/en-US/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 33 - 0AD ###############################################

html = requests.get('https://en.wikipedia.org/wiki/0_A.D._(video_game)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('"margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "0 A.D.", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Real-time strategy game", category="Games", license = 'GNU GPL v2+', repository = 'trac.wildfiregames.com/browser/ps/trunk/', website = 'https://play0ad.com/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 34 - SPEEDDREAMS ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Speed_Dreams')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('"margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Speed Dreams", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Racing car game", category="Games", license = 'Various', repository = '-', website = 'http://www.speed-dreams.org/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 35 - PIDGIN ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Pidgin_(software)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('<td>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Pidgin", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Multi-platform instant messaging", category="Internet", license = 'GNU GPL', repository = 'bitbucket.org/pidgin/main', website = 'https://pidgin.im/')
q.save()
dummy_count += 1
print("\nProject #",dummy_count, "parsed\n")

################### 36 - OPENCLONK ###############################################

html = requests.get('https://en.wikipedia.org/wiki/OpenClonk')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('"margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "OpenClonk", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "2D action game", category="Games", license = 'GNU GPL', repository = 'https://git.openclonk.org/openclonk.git', website = 'https://www.openclonk.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 37 - WIRESHARK ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Wireshark')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('"margin:0px;">(.+?)<sup class', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Wireshark", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Network protocol analyzer", category="Internet", license = 'GNU GPL', repository = 'code.wireshark.org/review/gitweb?p=wireshark.git', website = 'https://www.wireshark.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 38 - QGIS ###############################################

html = requests.get('https://en.wikipedia.org/wiki/QGIS')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('<td>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "QGIS", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Geographic information system", category="Geography", license = 'GNU GPL v2', repository = 'https://github.com/qgis/QGIS.git', website = 'https://qgis.org/en/site/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 39 - GNU CORE UTILITIES ###############################################

html = requests.get('https://en.wikipedia.org/wiki/GNU_Core_Utilities')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('<td>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "GNU coreutils", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "GNU core utilities", category="Operating System", license = 'GNU GPL v3+', repository = 'git.savannah.gnu.org/cgit/coreutils.git', website = 'https://www.gnu.org/software/coreutils/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 40 - VIRTUALBOX ###############################################

html = requests.get('https://en.wikipedia.org/wiki/VirtualBox')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('"margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "VirtualBox", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Operating system virtualization", category="Operating System", license = 'GNU GPL v2', repository = 'www.virtualbox.org/browser/vbox/trunk', website = 'https://www.virtualbox.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 41 -QTCREATOR ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Qt_Creator')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('"margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Qt Creator", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "C++/JavaScript/QML IDE", category="Development", license = 'LGPL', repository = 'code.qt.io/cgit/qt-creator/qt-creator.git/', website = 'https://www.qt.io/qt-features-libraries-apis-tools-and-ide/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 42 - KOLOURPAINT ###############################################

html = requests.get('https://en.wikipedia.org/wiki/KolourPaint')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('<td>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search(' <small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "KolourPaint", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Simple paint program", category="Graphics", license = 'BSD', repository = '-', website = 'https://www.kde.org/applications/graphics/kolourpaint/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 43 - FREECAD ###############################################

html = requests.get('https://en.wikipedia.org/wiki/FreeCAD')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('"margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "FreeCad", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "General-purpose parametric 3D CAD modeler", category="Graphics", license = 'GNU LGPL v2+', repository = 'https://github.com/FreeCAD/FreeCAD.git', website = 'https://www.freecadweb.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 44 - THUNDERBIRD ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Mozilla_Thunderbird')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('<td>(.+?) <small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search(' <small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Mozilla Thunderbird", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Email client", category="Office", license = 'MPL 2.0', repository = 'hg.mozilla.org/comm-central', website = 'https://www.thunderbird.net/en-GB/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 45 - MAKEHUMAN ###############################################

html = requests.get('https://en.wikipedia.org/wiki/MakeHuman')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('"margin:0px;">(.+?)<sup class', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/    (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "MakeHuman", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Prototyping of photo realistic humanoids", category="Graphics", license = 'GNU LGPL v2+', repository = 'bitbucket.org/MakeHuman/makehuman/commits/all', website = 'http://www.makehumancommunity.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 46 - HTOP ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Htop')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('"margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "htop", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Interactive system-monitor process-manager", category="Operating System", license = 'GNU GPL v2+', repository = 'https://github.com/hishamhm/htop.git', website = 'https://hisham.hm/htop/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 47 - RADIANCE ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Radiance_(software)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[3]

try:
    version_number = re.search('<td>(.+?)<small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)</small> <small class', str(coord_tmp)).group(1)
    release_date = release_date[1:]
    release_date = release_date[:-1]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Radiance", latest_version = version_number, release_date = datetime.strptime(release_date, '%Y-%m-%d').isoformat(), description = "A Validated Lighting Simulation Tool", category="Science", license = 'various', repository = '-', website = 'https://www.radiance-online.org//')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 48 - CLANG ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Clang')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('margin:0px;">(.+?)<sup class="r', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprin', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Clang", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "LLVM compiler", category="Development", license = '-', repository = '-', website = 'http://llvm.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 49 - CPYTHON ###############################################

html = requests.get('https://en.wikipedia.org/wiki/CPython')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('margin:0px;">(.+?)/', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprin', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "CPython", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Interperter for the Python programming language", category="Development", license = 'Python Software Foundation License', repository = 'github.com/python/cpython', website = 'https://www.python.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 50 - TRANSMISSION ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Transmission_(BitTorrent_client)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('<td>(.+?)<small>', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint">', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Transmission", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "BitTorrent client", category="Internet", license = 'various', repository = 'github.com/transmission/transmission', website = 'https://transmissionbt.com/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 51 - OPENSSH ###############################################

html = requests.get('https://en.wikipedia.org/wiki/OpenSSH')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('"margin:0px;">(.+?)<sup class="', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint">', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "OpenSSH", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Connectivity tool for remote login using SSH", category="Operating System", license = 'BSD', repository = '	https://github.com/openssh', website = 'https://www.openssh.com/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 52 - ONLYOFFICE ###############################################

html = requests.get('https://en.wikipedia.org/wiki/OnlyOffice')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[3]

try:
    version_number = re.search('margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprin', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "OnlyOffice", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Office suite", category="Office", license = 'GNU AGPL v3', repository = '-', website = 'https://www.onlyoffice.com/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 53 - BASH ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Bash_(Unix_shell)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('<td>(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprin', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

release_date = release_date[1:]

q = Project(name = "GNU Bash", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Unix shell", category="Operating System", license = 'GNU GPL v3', repository = 'git.savannah.gnu.org/cgit/bash.git', website = 'https://www.gnu.org/software/bash/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 54 - RSTUDIO ###############################################

html = requests.get('https://en.wikipedia.org/wiki/RStudio')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[11]

try:
    version_number = re.search('margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprin', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "R Studio", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "IDE for statistical computing using R programming language", category="Science", license = 'GNU AGPL v3', repository = 'github.com/rstudio/rstudio', website = 'https://www.rstudio.com/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 55 - GPG ###############################################

html = requests.get('https://en.wikipedia.org/wiki/GNU_Privacy_Guard')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('Modern</th><td>(.+?)/', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprin', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "GNU Privacy Guard", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Criptography software", category="Internet", license = 'GNU GPL v3', repository = 'dev.gnupg.org/source/gnupg/', website = 'https://gnupg.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 56 - VLC ###############################################

html = requests.get('https://en.wikipedia.org/wiki/VLC_media_player')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('macOS</th><td>(.+?)/', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprin', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "VLC media player", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Video player", category="Video", license = 'GNU LGPL v2.1+', repository = 'code.videolan.org/explore/projects/starred', website = 'https://www.videolan.org/vlc/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 57 - KDENLIVE ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Kdenlive')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('margin:0px;">(.+?)<sup class="reference" ', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprin', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Kdenlive", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Video editor", category="Video", license = 'GNU GPL v2+', repository = 'cgit.kde.org/kdenlive.git/', website = 'https://kdenlive.org/en/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 58 - OBS ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Open_Broadcaster_Software')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[7]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n ', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Open Broadcaster Software", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Screen recorder", category="Video", license = 'GNU GPL v2+', repository = 'github.com/obsproject/obs-studio', website = 'https://obsproject.com/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 59 - CHEESE ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Cheese_(software)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('<td>(.+?)<sup class="reference" ', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Cheese", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Webcam application", category="Video", license = 'GNU GPL', repository = 'github.com/obsproject/obs-studio', website = 'https://wiki.gnome.org/Apps/Cheese')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 60 - PARAVIEW ###############################################

html = requests.get('https://en.wikipedia.org/wiki/ParaView')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('style="margin:0px;">(.+?)<sup class="reference"', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "ParaView", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Application for interactive, scientific visualization.", category="Science", license = 'BSD', repository = '-', website = 'https://www.paraview.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 61 - OPENSHOT ###############################################

html = requests.get('https://en.wikipedia.org/wiki/OpenShot')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n ', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/    (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "OpenShot", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Video editor", category="Video", license = 'GNU GPL v3', repository = 'https://github.com/OpenShot/openshot-qt.git', website = 'https://www.openshot.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 62 -SHOTCUT ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Shotcut')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('style="margin:0px;">(.+?)<small', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Shotcut", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Video editor", category="Video", license = 'GNU GPL v3', repository = 'https://github.com/mltframework/shotcut.git', website = 'https://shotcut.org/blog/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 63 - ELMER ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Elmer_FEM_solver')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[3]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Elmer", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Finite element solver", category="Science", license = 'GNU GPL', repository = '-', website = 'https://www.csc.fi/web/elmer')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 64 - QUCS ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Quite_Universal_Circuit_Simulator')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Qucs", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Quite Universal Circuit Simulator", category="Science", license = 'GNU GPL v2+', repository = 'https://github.com/Qucs/qucs.git', website = 'http://qucs.sourceforge.net/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 65 - ECLISPE ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Eclipse_(software)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('style="margin:0px;">(.+?) ', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Eclipse", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Java IDE", category="Development", license = 'Eclipse Public License', repository = 'git.eclipse.org/c/', website = 'https://www.eclipse.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 66 - BLUEFISH ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Bluefish_(software)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Bluefish", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "IDE for web development", category="Development", license = 'GNU GPL', repository = 'sourceforge.net/p/bluefish/code/HEAD/tree/trunk/bluefish/', website = 'http://bluefish.openoffice.nl/index.html')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 67 - NETBEANS ###############################################

html = requests.get('https://en.wikipedia.org/wiki/NetBeans')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "NetBeans", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "IDE for Java", category="Development", license = 'GNU GPL', repository = 'github.com/apache/incubator-netbeans', website = 'http://netbeans.apache.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 68 - GDB ###############################################

html = requests.get('https://en.wikipedia.org/wiki/GNU_Debugger')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "GDB", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "GNU Debugger", category="Development", license = 'GNU GPL', repository = 'sourceware.org/git/gitweb.cgi?p=binutils-gdb.git', website = 'https://www.gnu.org/software/gdb/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 69 - GNUCASH ###############################################

html = requests.get('https://en.wikipedia.org/wiki/GnuCash')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[6]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "GnuCash", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Accounting software", category="Office", license = 'GNU GPL v2', repository = 'https://github.com/Gnucash/gnucash', website = 'http://www.gnucash.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 70 - GEPHI ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Gephi')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[5]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Gephi", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Graph analysis and visualization software", category="Science", license = 'GNU GPL', repository = 'https://github.com/gephi/gephi.git', website = 'https://gephi.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 71 - GITLAB ###############################################

html = requests.get('https://en.wikipedia.org/wiki/GitLab')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[28]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "GitLab CE", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Web-based Git-repository manager ", category="Development", license = 'MIT', repository = 'https://gitlab.com/gitlab-org/gitlab-ce', website = 'https://about.gitlab.com/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 72 - HEXCHAT ###############################################

html = requests.get('https://en.wikipedia.org/wiki/XChat')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[17]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "HexChat", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "IRC client", category="Internet", license = 'Various', repository = 'hexchat.github.io/downloads.html', website = 'https://hexchat.github.io/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 73 - ARDUINO ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Arduino#Software')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[13]

try:
    version_number = re.search('style="margin:0px;">(.+?)\n', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('/ (.+?)<span class="noprint', str(coord_tmp)).group(1)
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Arduino IDE", latest_version = version_number, release_date = datetime.strptime(release_date, '%d %B %Y').isoformat(), description = "Physical computing IDE", category="Development", license = 'GNU GPL/LGPL', repository = 'https://github.com/arduino/Arduino.git', website = 'https://www.arduino.cc/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 74 - TOR ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Tor_(anonymity_network)')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[65]

try:
    version_number = re.search('<td>(.+?)<sup class="refer', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Tor Browser", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "Web browser configured to protect your anonymity", category="Internet", license = 'GPL', repository = 'https://gitweb.torproject.org/tor.git', website = 'https://www.torproject.org/projects/torbrowser.html')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 75 - VALGRIND ###############################################

html = requests.get('https://en.wikipedia.org/wiki/Valgrind')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('td')[4]

try:
    version_number = re.search('<td>(.+?)<small', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('<small>(.+?)<span class="noprint', str(coord_tmp)).group(1)
    release_date = release_date[1:]
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Valgrind", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d, %Y').isoformat(), description = "memory debugging", category="Development", license = 'GPL', repository = 'sourceware.org/git/valgrind.git', website = 'http://www.valgrind.org/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")

################### 76 - MINETEST ###############################################

html = requests.get('https://www.minetest.net/')

soup = BeautifulSoup(html.text, 'lxml')

coord_tmp = soup.find_all('p')[4]

try:
    version_number = re.search('News:</strong> (.+?)released', str(coord_tmp)).group(1)
except AttributeError: # in case version number is not found
    version_number = 'version not found'

try:
    release_date = re.search('released. (.+?)</small>', str(coord_tmp)).group(1)
    day_month = release_date[:8]
    day_month = day_month[1:]
    year = release_date[-5:]
    year = year[:-1]
    release_date = day_month + ' ' + year
except AttributeError: # in case release date is not found
    release_date = 'release date not found'

q = Project(name = "Minetest", latest_version = version_number, release_date = datetime.strptime(release_date, '%B %d %Y').isoformat(), description = "Sandbox voxel game", category="Games", license = 'GPL', repository = 'https://github.com/minetest', website = 'https://www.minetest.net/')
q.save()
dummy_count += 1

print("\nProject #",dummy_count, "parsed\n")


# To do: Chromium and Tor
