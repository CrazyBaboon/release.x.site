# Release.X (C) Nuno Ferreira and contributors - Licensed under GNU AGPLv3+

from django.db import models


class Project(models.Model):
    name = models.CharField(max_length=200, default='-')
    latest_version = models.CharField(max_length=200, default='-')
    release_date = models.DateTimeField('date released')
    description = models.CharField(max_length=400, default='-')
    category = models.CharField(max_length=100, default='-')
    license = models.CharField(max_length=100, default='-')
    repository = models.CharField(max_length=400, default='-')
    website = models.CharField(max_length=400, default='-')
    
