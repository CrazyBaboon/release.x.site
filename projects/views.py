# Release.X (C) Nuno Ferreira and contributors - Licensed under GNU AGPLv3+

from django.http import HttpResponse
from django.shortcuts import get_object_or_404, render
from django.template import loader
from django.db.models import Q
from .models import Project
from django.shortcuts import redirect
from time import gmtime, strftime

def index(request):
	#project_list = Project.objects.order_by('name')

    current_time = strftime("%d %B %Y %H:%M:%S", gmtime()) # time of website deployment
    project_list_audio = Project.objects.filter(Q(category="Audio")).order_by('-release_date') 
    project_list_development = Project.objects.filter(Q(category="Development")).order_by('-release_date')
    project_list_games = Project.objects.filter(Q(category="Games")).order_by('-release_date') 	
    project_list_geography = Project.objects.filter(Q(category="Geography")).order_by('-release_date') 
    project_list_graphics = Project.objects.filter(Q(category="Graphics")).order_by('-release_date')
    project_list_internet = Project.objects.filter(Q(category="Internet")).order_by('-release_date')   
    project_list_office = Project.objects.filter(Q(category="Office")).order_by('-release_date')  
    project_list_operating_system = Project.objects.filter(Q(category="Operating System")).order_by('-release_date')      
    project_list_science = Project.objects.filter(Q(category="Science")).order_by('-release_date')
    project_list_video = Project.objects.filter(Q(category="Video")).order_by('-release_date')
        
    template = loader.get_template('projects/index.html')
    context = {'project_list_graphics': project_list_graphics, 'project_list_audio': project_list_audio
    , 'project_list_development': project_list_development, 'project_list_games': project_list_games
    , 'project_list_office': project_list_office, 'project_list_operating_system': project_list_operating_system
    , 'project_list_science': project_list_science, 'project_list_geography': project_list_geography
    , 'project_list_internet': project_list_internet, 'project_list_video': project_list_video
    , 'current_time': current_time}
    
    return HttpResponse(template.render(context, request))

def detail(request, project_id):

    name = Project.objects.get(pk=project_id).name
    description = Project.objects.get(pk=project_id).description

    # Remove the hours and seconds off the date (by deleting everything after the first space)
    # ~ separator = ' '
    # ~ release_date = release_date.split(separator, 1)[0]
    #return HttpResponse("%s is a %s." % (name , description) )
    
    return redirect(Project.objects.get(pk=project_id).website)
